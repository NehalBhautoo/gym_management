package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    public static void main(String[] args) {
        try {
            Server newServer = new Server();
            newServer.server(args);
        } catch (Exception e) {
            System.out.println("Server connection error");
        }
    }

    public void server(String[] args) {
        try {
            ServerSocket serverSocket = new ServerSocket(4999);
            while (true) {
                Socket socket = serverSocket.accept();
                System.out.println("Client connected: " + socket);
                new Thread(new ServerRunnable(socket)).start();
            }
        } catch (IOException e) {
            System.out.println("Error connecting to client");
        }
    }
}

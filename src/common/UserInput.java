package common;

import java.io.Serializable;

public class UserInput implements Serializable {
    private String bookID, clientID, trainerID, startTime, endTime, focus, duration;
    public UserInput(String bookID, String clientID, String trainerID, String startTime, String endTime, String focus, String duration) {
        this.bookID = bookID;
        this.clientID = clientID;
        this.trainerID = trainerID;
        this.startTime = startTime;
        this.endTime = endTime;
        this.focus = focus;
        this.duration = duration;
    }

    public String getBookID() {
        return bookID;
    }

    public void setBookID(String bookID) {
        this.bookID = bookID;
    }

    public String getClientID() {
        return clientID;
    }

    public void setClientID(String clientID) {
        this.clientID = clientID;
    }

    public String getTrainerID() {
        return trainerID;
    }

    public void setTrainerID(String trainerID) {
        this.trainerID = trainerID;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getFocus() {
        return focus;
    }

    public void setFocus(String focus) {
        this.focus = focus;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
package client;

import common.UserInput;

import java.util.ArrayList;

public class SetInputData {
    private static ArrayList<UserInput> inputs;

    public void setInputs(ArrayList<UserInput> inputs) {
        SetInputData.inputs = inputs;
    }

    public ArrayList<UserInput> getInputs() {
        return inputs;
    }
}

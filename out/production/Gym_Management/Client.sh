#!/bin/bash

export PATH_TO_FX=/home/nehal/Documents/lib/javafx-sdk-11.0.2/lib

javac --module-path $PATH_TO_FX --add-modules javafx.controls client/Client.java

javac --module-path $PATH_TO_FX --add-modules javafx.controls,javafx.fxml client/Client.java

java --module-path $PATH_TO_FX --add-modules javafx.controls client.Client
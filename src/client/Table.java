package client;

import common.Booking;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.control.TableView;

import java.util.ArrayList;

public class Table {
    private TableView<Booking> tableView;

    public void table(BorderPane layout) {
        TableColumn<Booking, String> bookingIdColumn = new TableColumn<>("BookingId");
        bookingIdColumn.setCellValueFactory(new PropertyValueFactory<>("bookingID"));

        TableColumn<Booking, String> clientIdColumn = new TableColumn<>("ClientId");
        clientIdColumn.setCellValueFactory(new PropertyValueFactory<>("clientId"));

        TableColumn<Booking, String> pTIdColumn = new TableColumn<>("TrainerId");
        pTIdColumn.setCellValueFactory(new PropertyValueFactory<>("trainerId"));

        TableColumn<Booking, String> startTimeColumn = new TableColumn<>("StartTime");
        startTimeColumn.setCellValueFactory(new PropertyValueFactory<>("startTime"));

        TableColumn<Booking, String> endTimeColumn = new TableColumn<>("EndTime");
        endTimeColumn.setCellValueFactory(new PropertyValueFactory<>("endTime"));

        TableColumn<Booking, String> durationColumn = new TableColumn<>("Duration");
        durationColumn.setCellValueFactory(new PropertyValueFactory<>("duration"));

        TableColumn<Booking, String> focusColumn = new TableColumn<>("Focus");
        focusColumn.setCellValueFactory(new PropertyValueFactory<>("focus"));

        tableView = new TableView<>();
        tableView.getColumns().addAll(bookingIdColumn, clientIdColumn, pTIdColumn, startTimeColumn, endTimeColumn, durationColumn, focusColumn);
        layout.setCenter(tableView);
        tableData();
    }

    private void tableData() {
        ObservableList<Booking> data = FXCollections.observableArrayList();
        BookingArray bookingArray = new BookingArray();
        ArrayList<Booking> bookings = bookingArray.getBookings();
        for(Booking booking : bookings) {
            System.out.println("\nBooking ID: " + booking.getBookingID() + 
                " Client ID: " + booking.getClientId() + 
                " pT ID: " + booking.getTrainerId() + 
                " Start Time: " + booking.getStartTime() + 
                " End Time: " + booking.getEndTime() + 
                " Duration: " + booking.getDuration() +
                " Focus: " + booking.getFocus());

            data.add(new Booking(booking.getBookingID(),
                    booking.getClientId(),
                    booking.getTrainerId(),
                    booking.getStartTime(),
                    booking.getEndTime(),
                    booking.getDuration(),
                    booking.getFocus()));
            tableView.setItems(data);
        }
    }

    private void addButtonToTable() {
    	TableColumn<Booking, Void> colBtn = new TableColumn("Action");
    	Callback<TableColumn<Booking, Void>, TableCell<Booking, Void>> cellFactory = new Callback<TableColumn<Booking, Void>, TableCell<Booking, Void>>() {
    		@Override
    		public TableCell<Booking, Void> call(final TableColumn<Data, Void> param) {
    			final TableCell<Booking, Void> cell = new TableCell<Booking, Void>() {
    				private final Button btn = new Button("Delete");
	    			{
	    				btn.setOnAction((ActionEvent event) -> {
	    					Booking booking = getTableView().getItem().get(getIndex());
	    					System.out.println("Selected Data to delete: " + booking);
	    				});
	    			}

    				@Override
    				public void updateItem(Void, item, boolean empty) {
    					if(empty) {
    						setGraphic(null);
    					} else {
    						setGraphic(btn);
    					}
    				}
    			};
    			return cell;
    		}
    	};

    colBtn.setCellFactory(cellFactory);
    tableView.getColumns().add(colBtn);
	}
}
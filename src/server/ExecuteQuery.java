package server;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import common.Booking;

public class ExecuteQuery {

    private Connection connection;
    private Statement statement;
    private ResultSet resultSet;

    public ArrayList<Booking> getBooking() {
        ArrayList<Booking> bookings = new ArrayList<>();
        try {
            connection = ConnectDB.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM booking");
            while (resultSet.next()) {
                Booking booking = new Booking(resultSet.getString("booking_id"),
                        resultSet.getString("client_id"),
                        resultSet.getString("pT_id"),
                        resultSet.getString("startTime"),
                        resultSet.getString("endTime"),
                        resultSet.getString("duration"),
                        resultSet.getString("focus"));
                bookings.add(booking);
            } //statement.close();
            //connection.close();
        } catch (SQLException e) {
            System.out.println("Error Executing query");
            e.printStackTrace();
        } return bookings;
    }
}

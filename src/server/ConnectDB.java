package server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectDB {
    public static Connection getConnection() throws SQLException {
        String dbURL = "jdbc:mysql://localhost/Gym";
        String userName = "nehal";
        String password = "nehal";

        Connection connection = DriverManager.getConnection(dbURL, userName, password);
        System.out.println("Database Connected");
        return connection;
    }
}

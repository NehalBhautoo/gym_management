package server;

import common.UserInput;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;

public class ServerRunnable implements Runnable {

    private Socket socket;

    public ServerRunnable(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        ExecuteQuery executeQuery = new ExecuteQuery();
        ArrayList<UserInput> userInputs;
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
            objectOutputStream.writeObject(executeQuery.getBooking());
            System.out.println("Data Sent to client");
            objectOutputStream.reset();

            // reading user-inputs from client
            try {
                // reading new booking details
                Object object = objectInputStream.readObject();
                userInputs = (ArrayList<UserInput>) object;
                System.out.println(userInputs.size());
                SaveBookingInput saveBookingInput = new SaveBookingInput();
                saveBookingInput.setUserInputs(userInputs);
                System.out.println("New booking received");
                if(objectInputStream.markSupported()) {
                    objectInputStream.reset();
                }
            } catch (ClassNotFoundException e) {
                System.out.println("Could not read user input");
            }
        } catch (IOException e) {
            System.out.println("Error with object stream");
        }
    }
}

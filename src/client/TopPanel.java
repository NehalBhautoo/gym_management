package client;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class TopPanel {
    public void buildTop(BorderPane layout) {
        BorderPane topLayout = new BorderPane();
        topLayout.setId("topContainer");

        HBox titleBox = new HBox();
        titleBox.setAlignment(Pos.TOP_LEFT);
        titleBox.setSpacing(150);
        titleBox.setId("titleBox");

        ImageView icon = null;
        try{
            icon = new ImageView(new Image(new FileInputStream("../src/assets/weightlifting.png")));
        } catch(FileNotFoundException e) {
            e.printStackTrace();
        }
        assert icon != null;
        icon.setFitHeight(45);
        icon.setFitWidth(45);
        Label title = new Label("Gym Management System", icon);
        title.setId("appTitle");

        titleBox.getChildren().addAll(title);

        topLayout.setCenter(titleBox);
        layout.setTop(topLayout);
    }
}
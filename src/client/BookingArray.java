package client;

import common.Booking;

import java.util.ArrayList;

public class BookingArray {
    private static ArrayList<Booking> bookings;

    public void setBookings(ArrayList<Booking> bookings) {
        BookingArray.bookings = bookings;
    }

    public ArrayList<Booking> getBookings() {
        return bookings;
    }
}

package server;

import common.UserInput;
import java.util.ArrayList;

public class SaveBookingInput {
    private static ArrayList<UserInput> userInputs;

    public void setUserInputs(ArrayList<UserInput> userInputs) {
        SaveBookingInput.userInputs = userInputs;
    }

    public ArrayList<UserInput> getUserInputs() {
        return userInputs;
    }
}

package client;

import common.Booking;
import common.UserInput;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

import javax.swing.*;

import java.util.ArrayList;

import static javafx.geometry.Pos.TOP_CENTER;

public class LeftPanel {

    private TextField book, clientId, trainerId, start, end;
    private ComboBox<String> comboBox;
    private DatePicker duration;

    public void buildLeft(BorderPane layout) {
        BorderPane leftLayout = new BorderPane();

        VBox buttonBox = new VBox();
        buttonBox.setMinWidth(100);
        buttonBox.setAlignment(TOP_CENTER);
        buttonBox.setId("buttonContainer");
        buttonBox.setSpacing(20);

        Button viewTrainers = new Button("List Booking");
        viewTrainers.setAlignment(Pos.BASELINE_LEFT);
        viewTrainers.setMaxWidth(Double.MAX_VALUE);

        Button addBooking = new Button("Add Booking");
        addBooking.setAlignment(Pos.BASELINE_LEFT);
        addBooking.setMaxWidth(Double.MAX_VALUE);
        addBooking.setOnAction((event) -> {
            try {
                newBooking();
            } catch(Exception ex) {
                System.out.println("Error Opening Window");
            }
        });

        Button listClient = new Button("List Client");
        listClient.setAlignment(Pos.BASELINE_LEFT);
        listClient.setMaxWidth(Double.MAX_VALUE);
        listClient.setOnAction((event) -> {
            try {
                //clientList();
            } catch(Exception ex) {
                ex.printStackTrace();
            }
        });

        buttonBox.getChildren().addAll(viewTrainers, addBooking, listClient);
        leftLayout.setCenter(buttonBox);
        layout.setLeft(leftLayout);
    }

    public void newBooking() {
        Stage addBooking = new Stage();
        addBooking.initModality(Modality.APPLICATION_MODAL);
        addBooking.setOnCloseRequest(e -> {
            addBooking.close();
            Platform.runLater(() -> new Interface().start(new Stage()));
        });

        Label label = new Label();
        label.setText("Add New Booking");

        GridPane userInput = new GridPane();
        userInput.add(label, 0, 0);

        Text bookId = new Text("Booking ID");
        book = new TextField();

        Text client = new Text("Client ID");
        clientId = new TextField();

        Text trainer = new Text("PT ID");
        trainerId = new TextField();

        Text startTime = new Text("Add Start time");
        start = new TextField();

        Text endTime = new Text("Add End time");
        end = new TextField();

        Text selectDuration = new Text("Choose end Date");
        duration = new DatePicker();

        Text focuses = new Text("Focus");
        String[] focus = {"Weight Lost", "Flexibility", "Muscle Gain"};
        comboBox = new ComboBox<>(FXCollections.observableArrayList(focus));

        Button buttonAdd = new Button("Add");
        buttonAdd.setOnAction(event -> sendInputData());

        GridPane gridPane = new GridPane();
        gridPane.setMinSize(500, 300);
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane.setVgap(5);
        gridPane.setHgap(5);
        gridPane.setAlignment(Pos.CENTER);

        gridPane.getChildren().add(userInput);

        gridPane.add(bookId, 0, 1);
        gridPane.add(book, 1, 1);

        gridPane.add(client, 0, 2);
        gridPane.add(clientId, 1, 2);

        gridPane.add(trainer, 0, 3);
        gridPane.add(trainerId, 1, 3);

        gridPane.add(startTime, 0, 4);
        gridPane.add(start, 1, 4);

        gridPane.add(endTime, 0, 5);
        gridPane.add(end, 1, 5);

        gridPane.add(selectDuration, 0, 6);
        gridPane.add(duration, 1, 6);

        gridPane.add(focuses, 0, 7);
        gridPane.add(comboBox, 1, 7);

        gridPane.add(buttonAdd, 0, 8);

        Scene scene = new Scene(gridPane);
        addBooking.setScene(scene);
        addBooking.show();
    }

    public void sendInputData() {
        String bookField = book.getText();
        String clientField = clientId.getText();
        String trainerField = trainerId.getText();
        String startField = start.getText();
        String endField = end.getText();
        String localDate = duration.getValue().toString();
        String focus = comboBox.getValue();

        BookingArray bookingArray = new BookingArray();
        ArrayList<Booking> bookings = bookingArray.getBookings();
        for(Booking booking : bookings) {
            if(bookField.equalsIgnoreCase("") || bookField.matches(booking.getBookingID())) {
                JOptionPane.showMessageDialog(null, "Enter BookingID!");
            } else if(clientField.equalsIgnoreCase("") || clientField.matches(booking.getClientId())) {
                JOptionPane.showMessageDialog(null, "Enter ClientID");
            } else if(trainerField.equalsIgnoreCase("")) {
                JOptionPane.showMessageDialog(null, "Enter Trainer ID");
            } else if(startField.equalsIgnoreCase("")) {
                JOptionPane.showMessageDialog(null, "Enter A starting time");
            } else if(endField.equalsIgnoreCase("")) {
                JOptionPane.showMessageDialog(null, "Enter an end time");
            } else if(localDate.equalsIgnoreCase("")) {
                JOptionPane.showMessageDialog(null, "Choose an ending Date");
            }else if(focus.equalsIgnoreCase("")) {
                JOptionPane.showMessageDialog(null, "Choose a training focus");
            } else {
                ArrayList<UserInput> userInputs = new ArrayList<>();
                userInputs.add(new UserInput(bookField, clientField, trainerField, startField, endField, localDate, focus));
                SetInputData setInputData = new SetInputData();
                setInputData.setInputs(userInputs);
                System.out.println("Sending Data to Server");
            }
        }
    }
}
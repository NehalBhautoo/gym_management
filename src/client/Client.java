package client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;

import common.Booking;
import common.UserInput;

public class Client {
    public static void main(String[] args) {
        Interface userInterface = new Interface();
        try {
            Socket socket = new Socket("localhost", 4999);
            ArrayList<Booking> bookings;
            ArrayList<UserInput> inputs;
            try {
                ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());

                //reading booking from server
                Object object = objectInputStream.readObject();
                System.out.println("Data Booking received from server");
                bookings = (ArrayList<Booking>) object;

                //setting all booking in an ArrayList<>
                // to class BookingArray
                BookingArray bookingArray  =new BookingArray();
                bookingArray.setBookings(bookings);
                if (objectInputStream.markSupported()) {
                    objectInputStream.reset();
                }
                //running user Interface
                userInterface.run();

                //sending booking input to server
                SetInputData setInputData = new SetInputData();
                inputs = setInputData.getInputs();
                objectOutputStream.writeObject(inputs);
                System.out.println("Data sent to server");
                objectOutputStream.reset();
            } catch (IOException e) {
                System.out.println("Error with Object Stream");
            } catch (ClassNotFoundException e) {
                System.out.println("Error with object");
            }
        } catch (IOException e) {
            System.out.println("Error connecting to Server");
        }
    }
}

package common;

import java.io.Serializable;

public class Booking implements Serializable {
    private String bookingID;
    private String clientId;
    private String trainerId;
    private String startTime;
    private String endTime;
    private String duration;
    private String focus;

    public Booking(String bookingID, String clientId, String trainerId, String startTime, String endTime, String duration, String focus) {
        this.bookingID = bookingID;
        this.clientId = clientId;
        this.trainerId = trainerId;
        this.startTime = startTime;
        this.endTime = endTime;
        this.duration = duration;
        this.focus = focus;
    }

    public String getBookingID() {
        return bookingID;
    }

    public void setBookingID(String bookingID) {
        this.bookingID = bookingID;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getTrainerId() {
        return trainerId;
    }

    public void setTrainerId(String trainerId) {
        this.trainerId = trainerId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getFocus() {
        return focus;
    }

    public void setFocus(String focus) {
        this.focus = focus;
    }

    @Override
    public String toString() {
        return "Booking{" +
                "bookingID='" + bookingID + '\'' +
                ", clientId='" + clientId + '\'' +
                ", trainerId='" + trainerId + '\'' +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", duration='" + duration + '\'' +
                ", focus='" + focus + '\'' +
                '}';
    }
}
